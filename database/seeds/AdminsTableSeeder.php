<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for ($i=0;$i<=30;$i++)
        {
           DB::table('admins')->insert([
              'firstname'=>$faker->firstName,
              'lastname'=>$faker->lastName,
              'email'=>$faker->unique()->email,
              'password'=>$faker->password
           ]);
        }
    }
}
