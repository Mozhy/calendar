<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use MaddHatter\LaravelFullcalendar\Facades\Calendar;
//use MaddHatter\LaravelFullcalendar\Calendar;

use App\Event;

class EventController extends Controller

{
    public function index()
    {
        $events = [];

        $data = Event::all();

        if($data->count()){

            foreach ($data as $key => $value) {

                $events[] = Calendar::event(

                    $value->title,

                    true,

                    new \DateTime($value->start_date),

                    new \DateTime($value->end_date.' +1 day'),

                    $value->id,
                    [
                        'color' => '#f05050',
//                        'url' => 'events',
                    ]

                );

            }

        }

        $calendar = Calendar::addEvents($events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
//            'firstDay' => 1,
            'isJalaali' => true,
            'isRTL' => true,
            'lang' => 'fa',
            'locale' => 'fa',
            'selectable' => true,
            'editable' => true,
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
//            'viewRender' => 'function() {alert("Callbacks!");}',
            'select' => 'function(start, end, allDay) {
             var title = prompt("Enter Event Title");
                    if(title)
                    {
                      var start = start.format();
                      var end = end.format();
                       console.log(start);
                       console.log(end);
                        $.ajax({
                            url:"events",
                            type:"POST",
                            dataType:"json",
                            data:{title:title,start:start,end:end,\'_token\': $(\'meta[name="csrf-token"]\').attr(\'content\')},
                            success:function(res)
                            {                                
                                swal("Added Successfully","success");
                                calendar.fullCalendar(\'refetchEvents\');
                            }
                        })
                    }}',
            'eventResize' => 'function(event){
                    var start = event.start.format();
                    var end = event.end.format();
                    var title = event.title;
                    var id = event.id;
                    $.ajax({
                        url:"/resize",
                        type:"POST",
                        data:{title:title, start:start, end:end, id:id,\'_token\': $(\'meta[name="csrf-token"]\').attr(\'content\')},
                        success:function(res){
//                            calendar.fullCalendar(\'refetchEvents\');
                            swal(\'Event Update\',"success");
                        }
                    })}',
            'eventDrop' => 'function(event){
                   var start = event.start.format("Y-MM-DD");
                    var end = event.end.format("Y-MM-DD");
                    var title = event.title;
                    var id = event.id;
                    $.ajax({
                        url:"/update",
                        type:"POST",
                        data:{title:title, start:start, end:end, id:id,\'_token\': $(\'meta[name="csrf-token"]\').attr(\'content\')},
                        success:function(res)
                        {
//                            calendar.fullCalendar(\'refetchEvents\');
                            swal("Event Updated","success");
                        }
                    });}',
            'eventClick' => 'function(event)
            {
            //swal({type:"error",title:"Ooops",text:"Are you sure you want to remove it?"});
            if(confirm("Are you sure you want to remove it?"))
                    {
                        var id = event.id;
                        $.ajax({
                            url:"/delete",
                            type:"POST",
                            datatype:"json",
                            data:{id:id,\'_token\': $(\'meta[name="csrf-token"]\').attr(\'content\')},
                            success:function(res)
                            {
                               
                               swal("Event Removed");
                               calendar.fullCalendar(\'refetchEvents\');
                            }
                        })
                    }
            }',
        ]);

        return view('calendar', compact('calendar'));
//        return view('calendar')->withCalendar($calendar);


    }

    public function insert(Request $request)
    {
        $event = new Event();
        $event->title = $request->title;
        $event->start_date = $request->start;
        $event->end_date = $request->end;
        $event->save();

        return json_encode(['message'=>'Added Successfully']);

    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $event = Event::findOrFail($id);
        $event->delete();
        return json_encode(['message'=>'Delete Successfully']);

    }

    public function update(Request $request)
    {
        $id = $request->id;
        $title = $request->title;
        $start = $request->start;
        $end = $request->end;

        $event = Event::findOrFail($id);
        $event->start_date = $start;
        $event->end_date = $end;

        $event->save();

        return json_encode(['message'=>'Updated Successfully']);

    }

    public function resize(Request $request)
    {
        $id = $request->id;
        $title = $request->title;
        $start = $request->start;
        $end = $request->end;

        $event = Event::findOrFail($id);
        $event->start_date = $start;
        $event->end_date = $end;

        $event->save();

        return json_encode(['message'=>'Resized Successfully']);

    }

}