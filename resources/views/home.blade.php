@extends('layouts.master')

@section('content')

    <div class="col-lg-8 col-sm-8 col-12">

        <div class="row">
            <img src="img/bgames.png">
            {{--<div id="demo" class="carousel slide" data-ride="carousel">--}}
                {{--<ul class="carousel-indicators">--}}
                    {{--@foreach($slider_images as $slider_image)--}}
                        {{--<li data-target="#demo" data-slide-to="{{ $loop->index }}" class="{{ $loop->first ? 'active' : '' }}"></li>--}}
                    {{--@endforeach--}}
                {{--</ul>--}}

                {{--<div class="carousel-inner">--}}
                    {{--@foreach($slider_images as $slider_image)--}}
                        {{--<div class="carousel-item {{ $loop->first ? 'active' : '' }}">--}}
                            {{--<img class="img-fluid" src="{{ $slider_image->image }}" alt="{{ $slider_image->description }}">--}}
                            {{--<div class="carousel-caption d-none d-md-block">--}}
                                {{--<h3>{{ $slider_image->description }}</h3>--}}
                                {{--<p>{{ $slider_image->description }}</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                {{--@endforeach--}}

                {{--<!-- Left and right controls -->--}}
                    {{--<a class="carousel-control-prev" href="#demo" data-slide="prev">--}}
                        {{--<span class="carousel-control-prev-icon"></span>--}}
                    {{--</a>--}}
                    {{--<a class="carousel-control-next" href="#demo" data-slide="next">--}}
                        {{--<span class="carousel-control-next-icon"></span>--}}
                    {{--</a>--}}

                {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                <div style="width: 200px;height: 50px;background-color: orangered;border-radius: 5px">
                    <h5 class="text-left text-white p-3">محبوب ترین پست ها</h5>
                </div>

                <div class="favorite-posts-div">
                    <img src="img/logo.jpg" class="w-100 h-50">
                    <p>محتوای بازی Hitman  در ماه دسامبر مشخص شد.</p>
                </div>
            </div>

            <div class="col-6">
                <div style="width: 200px;height: 50px;background-color: orangered;border-radius: 5px">
                    <h5 class="text-left text-white p-3">جدیدترین مطالب</h5>
                </div>

                <div class="row bg-warning">
                    <div class="col-12">
                    <div class="new-posts-div">
                        <img src="img/logo.jpg" class="w-100 h-50">
                        <p>محتوای بازی Hitman  در ماه دسامبر مشخص شد.</p>
                    </div>
                    </div>
                </div>
            </div>

            <div class="col-3">
                <div style="width: 200px;height: 50px;background-color: orangered;border-radius: 5px">
                    <h5 class="text-left text-white p-3">اطلاعیه جشنواره</h5>
                </div>

                <div class="favorite-posts-div">
                    <img src="img/logo.jpg" class="w-100 h-50">
                    <p>محتوای بازی Hitman  در ماه دسامبر مشخص شد.</p>
                </div>
            </div>
        </div>
    </div>

    @endsection