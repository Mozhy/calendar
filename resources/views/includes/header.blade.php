<div class="container-fluid">
    <div class="row border">
        <header id="header">
            <div class="main-menu">
                <div class="container">
                    <div class="row align-items-center d-flex">

                        <div class="col-12">
                            <nav class="navbar navbar-expand-lg navbar-default">
                                <a class="navbar-brand" href="/"><img src="/img/logo.jpg" style="width: 70px;height: 70px" alt="" title=""/></a>
                                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                                    <span><i class="fas fa-align-justify"></i></span>
                                </button>
                                <div class="collapse navbar-collapse justify-content-between" id="collapsibleNavbar" style="text-align: right">
                                    <ul class="navbar-nav ">
                                        <li class="nav-item ml-3 ">
                                            <a class="nav-link gray3-color" href="/">صفحه اصلی</a>
                                        </li>
                                        <li class="nav-item ml-3">
                                            <a class="nav-link gray3-color" href="/contact-us">تماس با ما</a>
                                        </li>
                                        <li class="nav-item ml-3">
                                            <a class="nav-link gray3-color" href="/about-us">درباره ما</a>
                                        </li>
                                        <li class="nav-item ml-3">
                                            <a class="nav-link gray3-color" href="#">همکاری با ما</a>
                                        </li>
                                        <li class="nav-item ml-3">
                                            <a class="nav-link gray3-color" href="/blog">وبلاگ</a>
                                        </li>
                                    </ul>

                                    <ul class="navbar-nav">
                                        @if(Auth::check())
                                            @csrf
                                            <li class="nav-item ">
                                                @if(Auth::user()->is_admin == 1)
                                                    <a class="gray3-color" href="/admin" ><i class="fal fa-user gray3-color"  style="font-size: 1.4rem"></i> {{Auth::user()->name}} خوش آمدید</a>
                                                @else
                                                    <a class="gray3-color" href="/dashboard" ><i class="fal fa-user gray3-color"  style="font-size: 1.4rem"></i> {{Auth::user()->name}} خوش آمدید</a>
                                                @endif
                                            </li>
                                            <li class="nav-item mr-2" style="border: 2px solid grey;border-radius: 6px;width: 57px;height:30px;padding: 5px">

                                                <i class="fal fa-cart-plus" style="color: green"></i>
                                            </li>
                                        @else
                                            <li class="nav-item">
                                                <a class="nav-link ml-lg-2 gray3-color" href="/login">ورود / ثبت نام</a>
                                            </li>

                                            <li class="nav-item" style="border: 2px solid grey;border-radius: 6px;width: 70px;height:35px;padding: 5px">
                                                <div class="row p-1">
                                                    <div style="border-radius: 45px;border: 2px solid #1ab80b;width: 20px;height: 20px;margin-right:10px;color: #00FF00">

                                                        <i style="color: grey;font-size: 13px;padding: 6px 5px">3</i>
                                                    </div>
                                                    <i class="fal fa-shopping-cart fa-2x" style="color:#1ab80b;margin-right: 11px;margin-top:-6px;width: 1.5rem "></i>
                                                </div>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                            </nav>
                        </div>

                    </div>
                </div>
            </div>
        </header>
    </div>
</div>
