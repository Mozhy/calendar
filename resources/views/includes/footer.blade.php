<!--footer Area-->
<footer>
    <div class="container">

        <div class="row">

            <div class="col-md-4 col-sm-6 col-xs-12">

                <ul class="menu">

                    <li>
                        <a href="#"></a>
                    </li>

                    <li>
                        <a href="/contact">تماس با ما</a>
                    </li>

                    <li>
                        <a href="#">تبلیغات</a>
                    </li>

                    <li>
                        <a href="#">پیشنهادات</a>
                    </li>

                    <li>
                        <a href="#">استخدام</a>
                    </li>

                    <li>
                        <a href="#">در خواست مشاوره </a>
                    </li>

                </ul>

            </div>


            <div class="col-md-4 col-sm-6 col-xs-12">

                <ul class="address">

                    <li>
                        <a href="#">جوایز</a>
                    </li>

                    <li>
                        <a href="#">ارتباط با واحد فنی</a>
                    </li>

                    <li>
                        <a href="#">دعوت نامه</a>
                    </li>

                    <li>
                        <a href="#">گالری تصاویر</a>
                    </li>


                </ul>


            </div>

            <div class="col-md-4 col-sm-6 col-xs-12">

                <span class="logo"><img src="/img/logo.png"></span>

                <span class="logo-title">بازی های جدید بی گیمز</span>

                <div class="clear"></div>

                <div class="search-text">

                    <div class="form">

                        <form id="search-form" class="form-search form-horizontal">
                            <div class="row">

                                <input type="text" class="input-search" placeholder="ایمیل خود را وارد نمایید">

                                <button type="submit" class="btn-search">تایید</button>

                            </div>

                        </form>

                    </div>


                </div>

            </div>

            <div class="line"></div>

            <div class="row">

                <div class="social">
                    <ul>
                        <li><a href="#"><i class="fa fa-lg fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-lg fa-youtube"></i></a></li>

                    </ul>
                </div>
                <div class="copy-right">

                    <h6>تمامی حقوق مادی و معنوی اين سايت متعلق به شرکت سپهر فناوران ميباشد.</h6>

                </div>
            </div>

        </div>

    </div>
</footer>
