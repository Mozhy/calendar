<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/fullcalendar.css" />
<link rel="stylesheet" href="css/sweetalert2.css">
<script src="js/jquery-2.19.2.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/moment-jalaali.js"></script>
<script src="js/fullcalendar.min.js"></script>
<script src="js/fa.js"></script>
<script src="js/sweetalert2.all.js"></script>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">تقویم شمسی</div>
                <div class="panel-body">
                    
                    {!! $calendar->calendar() !!}
                    {!! $calendar->script() !!}
                    
                    <meta name="csrf-token" content="{!! csrf_token() !!}">
                </div>
            </div>
        </div>
    </div>
</div>